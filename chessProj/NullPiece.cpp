#include "NullPiece.h"

NullPiece::NullPiece()
{
	this->_type = '#';
	this->_x_coords = MAX + 1;
	this->_y_coords = MAX + 1;
}

NullPiece::~NullPiece()
{
}

bool NullPiece::check_movement(int x_dest, int y_dest, Board& b)
{
	return false;
}

bool NullPiece::checkNullPiece(int col, int row, Board& b)
{
	return (b._pieces[row][col]->get_type() == '#');
}