#pragma once
#include "Piece.h"

#define BLACK_PIECES "rkbnpq"
#define WHITE_PIECES "RKBNPQ"
#define AMOUNT_OF_PIECES_TYPES 6

class Piece;
class Player
{
public:
	Player(int color);
	Player();
	~Player();

	bool check_piece(Piece& p)const; //checking if a piece is his piece if it is - returning true

private:
	std::string _pieces;
};
