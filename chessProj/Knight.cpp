#include "Knight.h"

Knight::Knight(int color, int x)
{
	GameExecptions::check_bound(x, 0); //exception if out of board
	this->_x_coords = x;
	if (color == WHITE)
	{
		this->_type = 'N';
		this->_y_coords = 0;
	}
	else
	{
		this->_type = 'n';
		this->_y_coords = MAX;
	}
}

Knight::~Knight()
{
}

bool Knight::check_movement(int x_dest, int y_dest, Board& b)
{
	if (this->_x_coords + 1 == x_dest && this->_y_coords + 2 == y_dest) return true;
	else if (this->_x_coords + 2 == x_dest && this->_y_coords + 1 == y_dest) return true;
	else if (this->_x_coords + 2 == x_dest && this->_y_coords - 1 == y_dest) return true;
	else if (this->_x_coords + 1 == x_dest && this->_y_coords - 2 == y_dest) return true;
	else if (this->_x_coords - 1 == x_dest && this->_y_coords - 2 == y_dest) return true;
	else if (this->_x_coords - 2 == x_dest && this->_y_coords - 1 == y_dest) return true;
	else if (this->_x_coords - 2 == x_dest && this->_y_coords + 1 == y_dest) return true;
	else if (this->_x_coords - 1 == x_dest && this->_y_coords + 2 == y_dest) return true;
	else return false;
}