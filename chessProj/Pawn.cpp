#include "Pawn.h"

Pawn::Pawn(int color, int x)
{
	GameExecptions::check_bound(x, 0); //exception if out of board
	this->_x_coords = x;
	if (color == WHITE)
	{
		this->_type = 'P';
		this->_y_coords = 1;
	}
	else
	{
		this->_type = 'p';
		this->_y_coords = MAX - 1;
	}
	this->_move_cnt = 0;
}

Pawn::~Pawn()
{
}

bool Pawn::check_movement(int x_dest, int y_dest, Board& b)
{
	if (NullPiece::checkNullPiece(x_dest, y_dest, b)) 
	{
		if ((y_dest == this->_y_coords + 2 || y_dest == this->_y_coords - 2) && this->_x_coords == x_dest && !this->_move_cnt) //checking if is the first move
		{
			if (LineMovePiece::checkBetween(false, LineMovePiece::checkStraight(this->_x_coords, this->_y_coords, x_dest, y_dest), this->_x_coords, this->_y_coords, x_dest, y_dest, b)) //checking if theres someone in the way
			{
				return inc_move();
			}
			else return false;
		}
		else if ((y_dest == this->_y_coords + 1 || y_dest == this->_y_coords - 1) && this->_x_coords == x_dest) //checking moving one block
		{
			return inc_move();
		}
		else return false;
	}
	else
	{
		if ((this->_y_coords + 1 == y_dest || this->_y_coords - 1 == y_dest) && (this->_x_coords - 1 == x_dest || this->_x_coords + 1 == x_dest)) //checking eating
		{
			return inc_move();
		}
		else if (this->_move_cnt == 1 && (this->_y_coords == PAWN_PLACE_WHITE_Y + 2 || this->_y_coords == PAWN_PLACE_BLACK_Y - 2) && (this->_y_coords - 1 == y_dest || this->_y_coords + 1 == y_dest) && this->_x_coords == x_dest) //checking en-passent
		{
			return inc_move();
		}
		else return false;
	}
	return false;
}

bool Pawn::inc_move()
{
	this->_move_cnt += 1;
	return true;
}