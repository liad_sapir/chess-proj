#include "Bishop.h"

Bishop::Bishop(int color, int x)
{
	GameExecptions::check_bound(x, 0); //exception if out of board
	this->_x_coords = x;
	if (color == WHITE)
	{
		this->_type = 'B';
		this->_y_coords = 0;
	}
	else
	{
		this->_type = 'b';
		this->_y_coords = MAX;
	}
}

Bishop::~Bishop()
{
}

bool Bishop::check_movement(int x_dest, int y_dest, Board& b)
{
	return (LineMovePiece::checkBetween(LineMovePiece::checkDiagonal(this->_x_coords, this->_y_coords, x_dest, y_dest),false,this->_x_coords, this->_y_coords, x_dest, y_dest, b));
}