#include "Board.h"
#include "Bishop.h"
#include "King.h"
#include "Queen.h"
#include "Rook.h"
#include "NullPiece.h"
#include "Knight.h"
#include "Pawn.h"

Board::Board()
{
	int i = 0, j = 0;
	
	//setting rooks

	this->_pieces[MAX][0] = new Rook(BLACK, 0);
	this->_pieces[MAX][MAX] = new Rook(BLACK, MAX);

	this->_pieces[0][0] = new Rook(WHITE, 0);
	this->_pieces[0][MAX] = new Rook(WHITE, MAX);

	//setting knights

	this->_pieces[MAX][KNIGHT_PLACE_LEFT] = new Knight(BLACK, KNIGHT_PLACE_LEFT);
	this->_pieces[MAX][KNIGHT_PLACE_RIGHT] = new Knight(BLACK, KNIGHT_PLACE_RIGHT);

	this->_pieces[0][KNIGHT_PLACE_LEFT] = new Knight(WHITE, KNIGHT_PLACE_LEFT);
	this->_pieces[0][KNIGHT_PLACE_RIGHT] = new Knight(WHITE, KNIGHT_PLACE_RIGHT);
	
	//setting bishops

	this->_pieces[MAX][BISHOP_PLACE_LEFT] = new Bishop(BLACK, BISHOP_PLACE_LEFT);
	this->_pieces[MAX][BISHOP_PLACE_RIGHT] = new Bishop(BLACK, BISHOP_PLACE_RIGHT);

	this->_pieces[0][BISHOP_PLACE_LEFT] = new Bishop(WHITE, BISHOP_PLACE_LEFT);
	this->_pieces[0][BISHOP_PLACE_RIGHT] = new Bishop(WHITE, BISHOP_PLACE_RIGHT);

	//setting kings

	this->_pieces[MAX][KING_PLACE_X] = new King(BLACK);

	this->_pieces[0][KING_PLACE_X] = new King(WHITE);

	//setting queens

	this->_pieces[MAX][QUEEN_PLACE_X] = new Queen(BLACK);

	this->_pieces[0][QUEEN_PLACE_X] = new Queen(WHITE);

	//setting pawns

	for (j = 0; j <= MAX; j++)
	{
		this->_pieces[PAWN_PLACE_WHITE_Y][j] = new Pawn(WHITE,j);
		this->_pieces[PAWN_PLACE_BLACK_Y][j] = new Pawn(BLACK,j);
	}

	//setting null pieces

	for (i = NULL_PIECE_START_Y; i <= NULL_PIECE_END_Y; i++)
	{
		for (j = 0; j <= MAX; j++)
		{
			this->_pieces[i][j] = new NullPiece();
		}
	}
}

Board::Board(Board& b)
{
	int i = 0, j = 0;
	for (i = 0; i <= MAX; i++) for (j = 0; j <= MAX; j++)
	{
		this->_pieces[i][j] = b._pieces[i][j];
	}
}

Board::~Board()
{
	int i = 0, j = 0;
	for (i = 0; i <= MAX; i++) 
	{
		for (j = 0; j <= MAX; j++)
		{
			delete this->_pieces[i][j];
		}
	}
}