#include "King.h"

King::King(int color)
{
	this->_x_coords = KING_PLACE_X;
	if (color == WHITE)
	{
		this->_type = 'K';
		this->_y_coords = 0;
	}
	else
	{
		this->_type = 'k';
		this->_y_coords = MAX;
	}
}

King::~King()
{
}

bool King::check_movement(int x_dest, int y_dest, Board& b)
{
	return ((x_dest >= (this->_x_coords - 1) && (x_dest <= this->_x_coords + 1)) && (y_dest >= (this->_y_coords - 1) && y_dest <= (this->_y_coords + 1)));
}