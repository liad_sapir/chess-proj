#include "GameExceptions.h"

int GameExecptions::_code_illegal = OK;

void GameExecptions::check_bound(int x, int y)
{
	if (!((x >= MIN && x <= MAX) && (y >= MIN && y <= MAX)))
	{
		throw Bound_Exception();
	}
}

void GameExecptions::check_matching_src_dst(int x_src, int y_src, int x_dst, int y_dst)
{
	if (x_src == x_dst && y_src == y_dst) throw Matching_Src_Dst();
}