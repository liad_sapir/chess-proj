#pragma once
#include <iostream>
#include "GameExceptions.h"
#include "Board.h"
#include "Helpers.h"

#define WHITE 0
#define BLACK 1

#define MAX 7

class Board;
class Piece
{
public:
	Piece();
	Piece(char type, int x, int y);
	virtual ~Piece();
	char get_type() const;
	void set_coords(int x_dest, int y_dest);
	virtual bool check_movement(int x_dest, int y_dest, Board& b) = 0; //checking if the movement of the piece is valid

protected:
	char _type;
	int  _x_coords;
	int  _y_coords;
};