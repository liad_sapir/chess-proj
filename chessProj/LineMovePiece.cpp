#include "LineMovePiece.h"

bool LineMovePiece::checkBetween(bool diagonal, bool straight ,int x_loc, int y_loc, int x_dest, int y_dest, Board& b)
{
	int x = 0, y = 0, maxVal = 0;
	if (diagonal)
	{
		if (x_loc > x_dest)
		{
			x = x_dest + 1;
			y = y_dest;
			if (y_loc > y_dest)
			{
				for (y = y + 1; x < x_loc; x++, y++)
				{
					if (!NullPiece::checkNullPiece(x, y, b)) return false;
				}
			}
			else
			{
				for (y = y - 1; x < x_loc; x++, y--)
				{
					if (!NullPiece::checkNullPiece(x, y, b)) return false;
				}
			}
		}
		else
		{
			x = x_loc + 1;
			y = y_loc;
			if (y_loc > y_dest)
			{
				for (y = y - 1; x < x_dest; x++, y--)
				{
					if (!NullPiece::checkNullPiece(x, y, b)) return false;
				}
			}
			else
			{
				for (y = y + 1; x < x_dest; x++, y++)
				{
					if (!NullPiece::checkNullPiece(x, y, b)) return false;
				}
			}
		}
		return true;
	}
	if (straight)
	{
		if (x_loc == x_dest)
		{
			for (y = Helpers::my_min(y_loc, y_dest) + 1; y < Helpers::my_max(y_loc, y_dest); y++) if (!NullPiece::checkNullPiece(x_loc, y, b)) return false;
		}
		else if (y_loc == y_dest)
		{
			for (x = Helpers::my_min(x_loc, x_dest) + 1; x < Helpers::my_max(x_loc, x_dest); x++) if (!NullPiece::checkNullPiece(x, y_loc, b)) return false;
		}
		return true;
	}
	else return false;
}

bool LineMovePiece::checkDiagonal(int x_loc, int y_loc, int x_dest, int y_dest)
{
	return (std::abs(x_loc - x_dest) == std::abs(y_loc - y_dest) != 0);
}

bool LineMovePiece::checkStraight(int x_loc, int y_loc, int x_dest, int y_dest)
{
	return ((x_loc == x_dest && y_loc != y_dest) || (x_loc != x_dest && y_loc == y_dest));
}