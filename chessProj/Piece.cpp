#include "Piece.h"

Piece::Piece(char type, int x, int y)
{
	this->_type = type;
	this->_x_coords = x;
	this->_y_coords = y;
}

Piece::Piece()
{
	this->_type = '#';
	this->_x_coords = MAX + 1;
	this->_y_coords = MAX + 1;
}

Piece::~Piece()
{
}

char Piece::get_type() const
{
	return this->_type;
}

void Piece::set_coords(int x_dest, int y_dest)
{
	this->_x_coords = x_dest;
	this->_y_coords = y_dest;
}
