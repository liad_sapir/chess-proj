#pragma once

#include "Piece.h"
#include "NullPiece.h"
#include "LineMovePiece.h"

class Pawn : public Piece
{
public:
	Pawn(int color, int x);
	~Pawn();
	virtual bool check_movement(int x_dest, int y_dest, Board& b);
	bool inc_move(); //increasing moves cnt;
private:
	int _move_cnt;
};

