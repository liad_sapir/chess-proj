#pragma once

#include "Piece.h"

class Knight : public Piece
{
public:
	Knight(int color, int x);
	~Knight();
	virtual bool check_movement(int x_dest, int y_dest, Board& b);
};

