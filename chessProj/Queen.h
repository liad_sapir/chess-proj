#pragma once

#include "Piece.h"
#include "LineMovePiece.h"

class Queen : public Piece
{
public:
	Queen(int color);
	~Queen();
	virtual bool check_movement(int x_dest, int y_dest, Board& b);
};