#pragma once
#include <exception>

#define MAX 7 
#define MIN 0

enum legal{OK, CHECK, NO_SRC_THIS, DST_THIS, CHECK_ON_THIS, OUT_OF_BOUNDS, BAD_MOVE, MATCHING_SRC_DST};

class GameExecptions
{
public:

	class No_Piece : public std::exception
	{
	public:
		virtual const char* what() const
		{
			_code_illegal = NO_SRC_THIS;
			return "none of your pieces are here!\n";
		}
	};

	class Piece_Dst : public std::exception
	{
	public:
		virtual const char* what() const
		{
			_code_illegal = DST_THIS;
			return "You already have a piece in the destination!\n";
		}
	};

	class Self_Check : public std::exception
	{
	public:
		virtual const char* what() const
		{
			_code_illegal = CHECK_ON_THIS;
			return "illegal move you made check on yourself!\n";
		}
	};

	class Bound_Exception : public std::exception
	{
	public:
		virtual const char* what() const
		{
			_code_illegal = OUT_OF_BOUNDS;
			return "out of bound!\n";
		}
	};

	static void check_bound(int x, int y); //returning true if the position is in bounds

	class Bad_Move : public std::exception
	{
	public:
		virtual const char* what() const
		{
			_code_illegal = BAD_MOVE;
			return "bad movment!\n";
		}
	};

	class Matching_Src_Dst : public std::exception
	{
	public:
		virtual const char* what() const
		{
			_code_illegal = MATCHING_SRC_DST;
			return "matching source and destination!\n";
		}
	};
	
	static void check_matching_src_dst(int x_src, int y_src, int x_dst, int y_dst); //exception if source and destination equal

	static int _code_illegal; 
};