#pragma once

#include <string>
#include "Board.h"
#include "Player.h"
#include "Pipe.h"
#include "Piece.h"
#include "NullPiece.h"

#define NUM_OF_PLAYERS 2

#define BLACK_INDEX 1
#define WHITE_INDEX 0

#define AM_OF_CHARS 66
#define ZERO_ASCII 48
#define QUIT "quit"
#define ERROR_LEN 2

class Board;
class Player;
class Game
{
public:
	Game();
	~Game();

	void move(std::string command);

	void sendBoard(Pipe p); //the string of board to send to fronted
	void handle_game(Pipe p); //the main function that connects the pipe

	void is_check(); //checking check

	void changeKingsPlace(int x_dest, int y_dest);
	
	void switch_turn(); //switching turns

private:
	int _x_white;
	int _y_white;

	int _x_black;
	int _y_black;

	Player _players[NUM_OF_PLAYERS];
	int _plays_id;

	Board* _board;
};