#pragma once

#include "Board.h"
#include "NullPiece.h"
#include "Helpers.h"

class Board;
/* A class for the pieces that move in straight or diagonal lines */
class LineMovePiece
{
public:
	/* This function checks if any piece is between this coords according to the movement */
	static bool checkBetween(bool diagonal, bool straight ,int x_loc, int y_loc, int x_dest, int y_dest, Board& b);
	/* Checking if the movement is diagonal */
	static bool checkDiagonal(int x_loc, int y_loc, int x_dest, int y_dest);
	/* Checking if the movement is straight */
	static bool checkStraight(int x_loc, int y_loc, int x_dest, int y_dest);
};

