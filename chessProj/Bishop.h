#pragma once

#include "Piece.h"
#include "LineMovePiece.h"

class Bishop : public Piece
{
public:
	Bishop(int color, int x);
	~Bishop();
	virtual bool check_movement(int x_dest, int y_dest, Board& b);
};