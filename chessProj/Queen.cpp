#include "Queen.h"

Queen::Queen(int color)
{
	this->_x_coords = QUEEN_PLACE_X;
	if (color == WHITE)
	{
		this->_type = 'Q';
		this->_y_coords = 0;
	}
	else
	{
		this->_type = 'q';
		this->_y_coords = MAX;
	}
}

Queen::~Queen()
{

}

bool Queen::check_movement(int x_dest, int y_dest, Board& b)
{
	return (LineMovePiece::checkBetween(false, LineMovePiece::checkStraight(this->_x_coords, this->_y_coords, x_dest, y_dest), this->_x_coords, this->_y_coords, x_dest, y_dest, b) ||
		    LineMovePiece::checkBetween(LineMovePiece::checkDiagonal(this->_x_coords, this->_y_coords, x_dest, y_dest), false, this->_x_coords, this->_y_coords, x_dest, y_dest, b));
}
