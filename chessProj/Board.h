#pragma once
#include <iostream>
#include <vector>
#include "Piece.h"

#define NULL_PIECE_START_Y 2
#define NULL_PIECE_END_Y 5

#define PAWN_PLACE_WHITE_Y 1
#define PAWN_PLACE_BLACK_Y 6

#define KNIGHT_PLACE_LEFT 1
#define KNIGHT_PLACE_RIGHT 6

#define BISHOP_PLACE_LEFT 2
#define BISHOP_PLACE_RIGHT 5

#define KING_PLACE_X 4

#define QUEEN_PLACE_X 3

class Piece;
class Board
{
public:
	Board();
	Board(Board& b);
	~Board();
	Piece* _pieces[MAX + 1][MAX + 1];
}; 
