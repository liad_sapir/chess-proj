#include "Rook.h"

Rook::Rook(int color, int x)
{
	GameExecptions::check_bound(x, 0); //exception if out of board
	this->_x_coords = x;
	if (color == WHITE)
	{
		this->_type = 'R';
		this->_y_coords = 0;
	}
	else
	{
		this->_type = 'r';
		this->_y_coords = MAX;
	}
}

Rook::~Rook()
{
}

bool Rook::check_movement(int x_dest, int y_dest, Board& b)
{
	return (LineMovePiece::checkBetween(false, LineMovePiece::checkStraight(this->_x_coords, this->_y_coords, x_dest, y_dest), this->_x_coords, this->_y_coords, x_dest, y_dest, b));
}