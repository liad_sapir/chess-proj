#pragma once
#include "Piece.h"

class King : public Piece
{
public:
	King(int color);
	~King();
	virtual bool check_movement(int x_dest, int y_dest, Board& b);
};

