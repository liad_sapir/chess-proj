#include "Player.h"

Player::Player(int color)
{
	if (color) this->_pieces = BLACK_PIECES;
	else this->_pieces = WHITE_PIECES;
}

Player::Player()
{

}

Player::~Player()
{

}

bool Player::check_piece(Piece& p) const
{
	return (this->_pieces.find(p.get_type()) != std::string::npos);
}