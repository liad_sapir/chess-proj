#pragma once
#include "Piece.h"

class NullPiece : public Piece
{
public:
	NullPiece();
	~NullPiece();
	virtual bool check_movement(int x_dest, int y_dest, Board& b);
	static bool checkNullPiece(int col, int row, Board& b); //checking if there is a null piece in the given place in the board
};
