#include "Game.h"

Game::Game()
{
	//setting a new board
	this->_board = new Board();
	//setting king's coords
	this->_x_black = this->_x_white = KING_PLACE_X;
	this->_y_black = MAX;
	this->_y_white = MIN;
	//setting players
	this->_players[BLACK_INDEX] = Player(BLACK_INDEX);
	this->_players[WHITE_INDEX] = Player(WHITE_INDEX);
	//black player starts the game
	this->_plays_id = BLACK;
}

Game::~Game()
{
}

void Game::handle_game(Pipe p)
{
	std::string msg_from_graphics = "";
	char* error_msg_to_graphics = new char[ERROR_LEN];

	this->sendBoard(p);
	msg_from_graphics = p.getMessageFromGraphics();

	while (msg_from_graphics != QUIT)
	{
		this->move(msg_from_graphics);

		*(error_msg_to_graphics) = GameExecptions::_code_illegal + ZERO_ASCII;
		*(error_msg_to_graphics + 1) = '\0';

		p.sendMessageToGraphics(error_msg_to_graphics);

		msg_from_graphics = p.getMessageFromGraphics();
	}
	delete[] error_msg_to_graphics;
	p.close();
}

void Game::sendBoard(Pipe p)
{
	int cnt = 0;
	char* output = new char[AM_OF_CHARS];
	int i = 0, j = 0;
	for (i = MAX; i >= MIN; i--) for (j = MIN; j <= MAX; j++)
	{
		*(output + cnt) = this->_board->_pieces[i][j]->get_type();
		cnt += 1;
	}
	*(output + AM_OF_CHARS - 2) = (char)(BLACK + ZERO_ASCII);
	*(output + AM_OF_CHARS - 1) = '\0';
	p.sendMessageToGraphics(output);
	delete[] output;
}

void Game::move(std::string command)
{
	Board* before_move = new Board(*this->_board); //saving board before movement
	int x_loc, y_loc, x_dest, y_dest;

	x_loc = command[0] - 'a'; 
	y_loc = command[1] - ZERO_ASCII - 1;
	std::cout << this->_board->_pieces[y_loc][x_loc]->get_type() << std::endl;
	
	x_dest = command[2] - 'a';
	y_dest = command[3] - ZERO_ASCII - 1;
	std::cout << this->_board->_pieces[y_dest][x_dest]->get_type() << std::endl;

	GameExecptions::_code_illegal = OK;

	/* Checking Legal Movement and making the move: */
	try
	{
		GameExecptions::check_bound(x_dest, y_dest);
		GameExecptions::check_matching_src_dst(x_loc, y_loc, x_dest, y_dest);
		if (!this->_players[this->_plays_id].check_piece(*this->_board->_pieces[y_loc][x_loc])) throw GameExecptions::No_Piece();
		if (this->_players[this->_plays_id].check_piece(*this->_board->_pieces[y_dest][x_dest])) throw GameExecptions::Piece_Dst();
		if (this->_board->_pieces[y_loc][x_loc]->check_movement(x_dest, y_dest, *(this->_board))) //checking if the piece movement is valid
		{
			this->_board->_pieces[y_loc][x_loc]->set_coords(x_dest, y_dest); //setting the new coords for the piece
			this->_board->_pieces[y_dest][x_dest] = this->_board->_pieces[y_loc][x_loc];  //switiching pieces
			this->_board->_pieces[y_loc][x_loc] = new NullPiece(); //setting nullPiece in the place the piece was before
			changeKingsPlace(x_dest, y_dest);
			try
			{
				this->is_check();
			}
			catch (std::exception& e)
			{
				e.what();
				this->_board = new Board(*before_move);
				changeKingsPlace(x_loc, y_loc);
				return;
			}
			switch_turn(); //switching turns
		}
		else throw GameExecptions::Bad_Move();
	}
	catch (std::exception& e)
	{
		e.what();
	}

}

void Game::is_check()
{
	int i = 0;
	int j = 0;
	for (i = 0; i <= MAX; i++)for (j = 0; j <= MAX; j++)
	{
		if (this->_players[BLACK].check_piece(*this->_board->_pieces[i][j])) //checking if it is a black piece
		{
			if (this->_board->_pieces[i][j]->check_movement(this->_x_white, this->_y_white, *this->_board)) //checking if a piece is able to eat the white king
			{
				if (this->_plays_id == WHITE) throw GameExecptions::Self_Check(); 
				else GameExecptions::_code_illegal = CHECK;
			}
		}
		else if (this->_players[WHITE].check_piece(*this->_board->_pieces[i][j])) //checking if it is a white piece
		{
			if (this->_board->_pieces[i][j]->check_movement(this->_x_black, this->_y_black, *this->_board)) //checking if a piece is able to eat the black king
			{
				if (this->_plays_id == BLACK) throw GameExecptions::Self_Check();
				else GameExecptions::_code_illegal = CHECK;
			}
		}
	}
}

void Game::changeKingsPlace(int x_dest, int y_dest)
{
	if (this->_board->_pieces[y_dest][x_dest]->get_type() == 'K')
	{
		this->_x_white = x_dest;
		this->_y_white = y_dest;
	}
	else if (this->_board->_pieces[y_dest][x_dest]->get_type() == 'k')
	{
		this->_x_black = x_dest;
		this->_y_black = y_dest;
	}
}

void Game::switch_turn()
{
	if (this->_plays_id == WHITE) this->_plays_id = BLACK; 
	else this->_plays_id = WHITE;
}