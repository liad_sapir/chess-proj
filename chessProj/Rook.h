#pragma once
#include "Piece.h"
#include "LineMovePiece.h"

class Rook : public Piece
{
public:
	Rook(int color, int x);
	~Rook();
	virtual bool check_movement(int x_dest, int y_dest, Board& b);
};